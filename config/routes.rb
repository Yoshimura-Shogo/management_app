# == Route Map
#
#                    Prefix Verb   URI Pattern                                                                              Controller#Action
#          new_user_session GET    /users/sign_in(.:format)                                                                 users/sessions#new
#              user_session POST   /users/sign_in(.:format)                                                                 users/sessions#create
#      destroy_user_session DELETE /users/sign_out(.:format)                                                                users/sessions#destroy
#         new_user_password GET    /users/password/new(.:format)                                                            users/passwords#new
#        edit_user_password GET    /users/password/edit(.:format)                                                           users/passwords#edit
#             user_password PATCH  /users/password(.:format)                                                                users/passwords#update
#                           PUT    /users/password(.:format)                                                                users/passwords#update
#                           POST   /users/password(.:format)                                                                users/passwords#create
#  cancel_user_registration GET    /users/cancel(.:format)                                                                  users/registrations#cancel
#     new_user_registration GET    /users/sign_up(.:format)                                                                 users/registrations#new
#    edit_user_registration GET    /users/edit(.:format)                                                                    users/registrations#edit
#         user_registration PATCH  /users(.:format)                                                                         users/registrations#update
#                           PUT    /users(.:format)                                                                         users/registrations#update
#                           DELETE /users(.:format)                                                                         users/registrations#destroy
#                           POST   /users(.:format)                                                                         users/registrations#create
#                      root GET    /                                                                                        home_pages#home
#       the_absent_students GET    /students/the_absent(.:format)                                                           students#the_absent_index
#    supplementary_students GET    /students/supplementary(.:format)                                                        students#supplementary_index
#                  students GET    /students(.:format)                                                                      students#index
#                           POST   /students(.:format)                                                                      students#create
#               new_student GET    /students/new(.:format)                                                                  students#new
#              edit_student GET    /students/:id/edit(.:format)                                                             students#edit
#                   student GET    /students/:id(.:format)                                                                  students#show
#                           PATCH  /students/:id(.:format)                                                                  students#update
#                           PUT    /students/:id(.:format)                                                                  students#update
#                           DELETE /students/:id(.:format)                                                                  students#destroy
#                  subjects GET    /subjects(.:format)                                                                      subjects#index
#                           POST   /subjects(.:format)                                                                      subjects#create
#               new_subject GET    /subjects/new(.:format)                                                                  subjects#new
#              edit_subject GET    /subjects/:title/edit(.:format)                                                          subjects#edit
#                   subject GET    /subjects/:title(.:format)                                                               subjects#show
#                           PATCH  /subjects/:title(.:format)                                                               subjects#update
#                           PUT    /subjects/:title(.:format)                                                               subjects#update
#                           DELETE /subjects/:title(.:format)                                                               subjects#destroy
#               absent_days GET    /absent_days(.:format)                                                                   absent_days#index
#                           POST   /absent_days(.:format)                                                                   absent_days#create
#            new_absent_day GET    /absent_days/new(.:format)                                                               absent_days#new
#           edit_absent_day GET    /absent_days/:id/edit(.:format)                                                          absent_days#edit
#                absent_day GET    /absent_days/:id(.:format)                                                               absent_days#show
#                           PATCH  /absent_days/:id(.:format)                                                               absent_days#update
#                           PUT    /absent_days/:id(.:format)                                                               absent_days#update
#                           DELETE /absent_days/:id(.:format)                                                               absent_days#destroy
#        rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
# rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#        rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
# update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#      rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations',
                                   passwords: 'users/passwords' }
  root to: 'home_pages#home'
  resources :users, only: [:index, :destroy]

  resources :students do
    collection do
      get '/the_absent' => 'students#the_absent_index'
      get '/supplementary' => 'students#supplementary_index'
    end
  end
  resources :subjects, param: :title
  resources :absent_days
end

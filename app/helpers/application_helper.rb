module ApplicationHelper
  def full_title(page_title: "")
    base_title = "MANAGEMENT APP"
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # 生徒が選択している科目にはtrueを返す
  def selected_subject?(student, subject)
    student.subjects.any? { |student_subject| student_subject.title == subject }
  end

  # 学年によって見出しを変える
  def index_title_for_students(grade, students)
    if grade.present?
      "中#{grade}生徒一覧（#{students.count}人）"
    else
      "生徒一覧（#{students.count}人）"
    end
  end

  # 曜日によって見出しを変える
  def index_title_for_subjects(day)
    if day.present?
      "科目一覧(#{day})"
    else
      "科目一覧"
    end
  end

end

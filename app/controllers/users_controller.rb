class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def destroy
    User.find(params[:id]).delete
    flash[:info] = "削除しました。"
    redirect_to users_path
  end
end

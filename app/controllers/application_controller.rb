class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  private

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    end

    def check_admin
      if user_signed_in?
        unless current_user.admin?
          redirect_to root_path
          flash[:notice] = "管理者のみ実行できます"
        end
      end
    end
end

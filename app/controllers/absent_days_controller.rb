class AbsentDaysController < ApplicationController
  def new
    @student = Student.find(params[:student_id])
    @absent_day = @student.absent_days.build
  end

  def create
    @student = Student.find(params[:student_id])
    @absent_day = @student.absent_days.build(absent_day_params)
    if @absent_day.save
      flash[:info] = "欠席情報を作成しました。"
      redirect_to the_absent_students_path
    else
      render 'absent_days/new'
    end
  end

  def update
    @absent_day = AbsentDay.find(params[:id])
    @absent_day.update_attribute(:supplementary, true)
    flash[:info] = "#{@absent_day.student_name}の補習を完了しました。"
    redirect_to supplementary_students_path
  end

  def destroy
    AbsentDay.find(params[:id]).delete
    flash[:info] = "欠席を取り消しました。"
    redirect_to the_absent_students_path
  end

  private
    def absent_day_params
      params.require(:absent_day).permit(:date, :subject)
    end
end

class HomePagesController < ApplicationController
  def home
    @subjects = Subject.day_of_today
    @the_absents = Student.joins(:absent_days).where(absent_days: { date: Date.today })
    @the_supplementaries = Student.joins(:absent_days).where(absent_days: { supplementary: false }).distinct
  end
end

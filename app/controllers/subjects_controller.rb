class SubjectsController < ApplicationController
  def index
    if params[:day].present?
      @subjects = Subject.where(day: params[:day])
    else
      @subjects = Subject.all
    end
  end

  def show
    @subject = Subject.find_by(title: params[:title])
  end
end

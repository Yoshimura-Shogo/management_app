class StudentsController < ApplicationController
  before_action :check_admin, only: [:new, :create, :edit, :update, :destroy]


  def index
    if params[:grade].present?
      @search = Student.ransack(params[:q])
      @students = @search.result.where(grade_id: params[:grade]).page(params[:page]).order(:grade_id)
      @all_students = @search.result.where(grade_id: params[:grade])
    else
      @search = Student.ransack(params[:q])
      @students = @search.result.page(params[:page]).per(10).order(:grade_id)
      @all_students = @search.result
    end
  end

  def show
    @student = Student.find(params[:id])
  end

  def index_by_grades
    @students = Student.where("grade_id = ?", params[:id])
  end

  def supplementary_index
    @absent_days = AbsentDay.includes(:student).supplementary
  end

  def the_absent_index
    @absent_days = AbsentDay.includes(:student)
  end

  def new
    @student = Student.new
  end

  def create
    set_params_for_registeration
    @student = Student.new(student_params)
    if @student.save
      add_subject if params[:student][:subject].present?
      flash[:success] = "新規生徒を登録しました。"
      redirect_to student_path(@student.id)
    else
      render 'new'
    end
  end

  def edit
    @student = Student.find(params[:id])
  end

  def update
    set_params_for_registeration
    @student = Student.find(params[:id])
    if @student.update_attributes(student_params)
      remove_subject
      add_subject if params[:student][:subject].present?
      flash[:success] = "生徒情報を編集しました"
      redirect_to student_path(@student.id)
    else
      render 'edit'
    end
  end

  def destroy
    Student.find(params[:id]).destroy
    flash[:success] = "生徒情報を削除しました。"
    redirect_to students_path
  end

  private
    def student_params
      params.require(:student).permit(:name, :grade, :grade_id)
    end

    def set_params_for_registeration
      case params[:student][:grade]
      when '中１'
        params[:student][:grade_id] = 1
      when '中２'
        params[:student][:grade_id] = 2
      when '中３'
        params[:student][:grade_id] = 3
      end
    end

    def add_subject
      params[:student][:subject].each do |subject|
        subject = Subject.find_by(title: subject)
        @student.add_subject(subject)
      end
    end

    def remove_subject
      @student.subjects.each do |subject|
        @student.remove_subject(subject)
      end
    end
end

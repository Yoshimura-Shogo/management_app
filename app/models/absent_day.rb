# == Schema Information
#
# Table name: absent_days
#
#  id            :integer          not null, primary key
#  date          :date             not null
#  subject       :string           not null
#  supplementary :boolean          default(FALSE)
#  student_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class AbsentDay < ApplicationRecord
  belongs_to :student
  validates :student_id, presence: true
  validates :date, presence: true
  validates :subject, presence: true

  delegate :id, :name, :grade, to: :student, prefix: true

  default_scope { order(date: :desc) }
  scope :supplementary , -> { where(absent_days: { supplementary: false }) }

  SUBJECTS = ["英語", "数学", "国語"]
end

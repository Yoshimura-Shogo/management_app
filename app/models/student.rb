# == Schema Information
#
# Table name: students
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  grade      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  grade_id   :integer
#

class Student < ApplicationRecord
  has_many :subject_students, dependent: :destroy
  has_many :subjects, through: :subject_students

  has_many :absent_days, dependent: :destroy

  validates :name, presence: true, length: { maximum: 20 }
  validates :grade, presence: true, length: { maximum: 10 }
  validates :grade_id, presence: true

  GRADES = ["中１", "中２", "中３"]


  def add_subject(subject)
    subjects << subject
  end

  def remove_subject(subject)
    subject_students.find_by(subject_id: subject.id).destroy
  end
end

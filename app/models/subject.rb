# == Schema Information
#
# Table name: subjects
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  day        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Subject < ApplicationRecord
  has_many :subject_students, dependent: :destroy
  has_many :students, through: :subject_students

  validates :title, presence: true
  validates :day, presence: true

  SUBJECTS = ["中１　英語A", "中１　英語B", "中１　数学A", "中１　数学B", "中１　国語A", "中１　国語B",
              "中２　英語A", "中２　英語B", "中２　数学A", "中２　数学B", "中２　国語A", "中２　国語B",
              "中３　英語A", "中３　英語B", "中３　数学A", "中３　数学B", "中３　国語A", "中３　国語B"]


  scope :day_of_today, -> { where(day: I18n.l(Date.today, format: :day)) }
end

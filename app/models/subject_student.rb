# == Schema Information
#
# Table name: subject_students
#
#  id         :integer          not null, primary key
#  subject_id :integer          not null
#  student_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SubjectStudent < ApplicationRecord
  belongs_to :student
  belongs_to :subject

  validates :student_id, presence: true
  validates :subject_id, presence: true
end

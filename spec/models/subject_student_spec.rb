# == Schema Information
#
# Table name: subject_students
#
#  id         :integer          not null, primary key
#  subject_id :integer          not null
#  student_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SubjectStudent, type: :model do
  let!(:student) { create(:student) }
  let!(:subject) { create(:subject) }
  before do
    @subject_student = SubjectStudent.new(subject_id: subject.id, student_id: student.id)
  end

  it 'is valid with both ids' do
    expect(@subject_student).to be_valid
  end

  it 'is invalis without subject id' do
    @subject_student.subject_id = nil
    expect(@subject_student).to be_invalid
  end

  it 'is invalis without student id' do
    @subject_student.student_id = nil
    expect(@subject_student).to be_invalid
  end
end

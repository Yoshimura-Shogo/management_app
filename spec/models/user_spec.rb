# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string           default(""), not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'is valid with name, email, and encrypted_password' do
    user = User.create( name: "user", email: "user@example.com", password: "password", password_confirmation: "password" )
    expect(user).to be_valid
  end

  it 'is invalid without name' do
    user = User.create(name: nil)
    user.valid?
    expect(user.errors[:name]).to include("を入力してください")
  end

  it 'is invalid without email' do
    user = User.create(email: nil)
    user.valid?
    expect(user.errors[:email]).to include("を入力してください")
  end

  it 'is invalid without password' do
    user = User.create(password: nil)
    user.valid?
    expect(user.errors[:password]).to include("を入力してください")
  end
end

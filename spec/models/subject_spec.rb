# == Schema Information
#
# Table name: subjects
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  day        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Subject, type: :model do
  it 'is valid with title and day' do
    subject = Subject.new(title: "英語", day: "月")
    expect(subject).to be_valid
  end

  it 'is invalid without title' do
    subject = Subject.new(title: "")
    subject.valid?
    expect(subject.errors[:title]).to include("を入力してください")
  end

  it 'is invalid without day' do
    subject = Subject.new(day: "")
    subject.valid?
    expect(subject.errors[:day]).to include("を入力してください")
  end
end

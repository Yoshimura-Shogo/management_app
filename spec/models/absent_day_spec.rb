# == Schema Information
#
# Table name: absent_days
#
#  id            :integer          not null, primary key
#  date          :date             not null
#  subject       :string           not null
#  supplementary :boolean          default(FALSE)
#  student_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe AbsentDay, type: :model do
  let!(:student) { create(:student) }
  let!(:absent_day) { create(:absent_day, date: Time.current.ago(1.day)) }
  let!(:absent_day_2) { create(:absent_day, date: Time.current, supplementary: true) }

  it 'is valid with date and subject' do
    absent_day = student.absent_days.create(date: Time.current, subject: "英語")
    expect(absent_day).to be_valid
  end

  it 'is invalid without date' do
    absent_day = student.absent_days.create(date: nil)
    absent_day.valid?
    expect(absent_day.errors[:date]).to include("を入力してください")
  end

  it 'is invalid without subject' do
    absent_day = student.absent_days.create(subject: nil)
    absent_day.valid?
    expect(absent_day.errors[:subject]).to include("を入力してください")
  end

  it 'is invalid without student_id' do
    absent_day = student.absent_days.create(student_id: nil)
    expect(absent_day).to be_invalid
  end

  it 'is most recent date first' do
    expect(AbsentDay.first).to eq absent_day_2
  end

  it 'scopes supplementary' do
    expect(AbsentDay.supplementary).to include absent_day
    expect(AbsentDay.supplementary).to_not include absent_day_2
  end
end

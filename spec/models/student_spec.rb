# == Schema Information
#
# Table name: students
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  grade      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  grade_id   :integer
#

require 'rails_helper'

RSpec.describe Student, type: :model do
  it 'is valid with name and grade' do
    student = Student.new(name: "example", grade: "中３", grade_id: 3)
    expect(student).to be_valid
  end

  it 'is invalid without name' do
    student = Student.new(name: nil)
    student.valid?
    expect(student.errors[:name]).to include("を入力してください")
  end

  it 'is invalid without grade' do
    student = Student.new(grade: nil)
    student.valid?
    expect(student.errors[:grade]).to include("を入力してください")
  end

  it 'is invalid without grade_id' do
    student = Student.new(grade_id: nil)
    student.valid?
    expect(student.errors[:grade_id]).to include("を入力してください")
  end

  it 'is invalid with too long name' do
    student = Student.new(name: "a"*21)
    student.valid?
    expect(student.errors[:name]).to include("は20文字以内で入力してください")
  end

  it 'is invalid with too long grade' do
    student = Student.new(grade: "a"*11)
    student.valid?
    expect(student.errors[:grade]).to include("は10文字以内で入力してください")
  end

  it 'adds and removes subject' do
    student = Student.create(id:5, name: "test", grade: "中３", grade_id: 3)
    subject = Subject.create(id:5, title: "中３　英語", day: "木")

    expect(student.subjects).to_not include subject
    student.add_subject(subject)
    expect(student.subjects).to include subject
    student.remove_subject(subject)
    expect(student.reload.subjects).to_not include subject
  end
end

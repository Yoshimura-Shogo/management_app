require 'rails_helper'

RSpec.describe "absent day", type: :request do
  let!(:absent_day) { create(:absent_day) }
  let!(:student) { create(:student) }
  login

  describe "GET /absent_days/new" do
    it "works successfully" do
      get new_absent_day_path(student_id: student.id)
      expect(response).to have_http_status(200)
      expect(response.body).to include student.name
      expect(response.body).to include student.grade
    end
  end

  describe "POST /absent_days" do
    it "creates new absent day" do
      expect do
        post absent_days_url, params:
                        { absent_day: { date: Date.today , subject: "英語" }, student_id: student.id }
      end.to change(AbsentDay, :count).by(1)
    end
  end

  describe "PUT /absent_day/:id" do
    it "updates supplementary attribute" do
      expect do
        put absent_day_url(absent_day.id)
      end.to change { AbsentDay.find(absent_day.id).supplementary }.from(false).to(true)
    end
  end

  describe "DELETE /absent_day/:id" do
    it "delete absent day" do
      expect do
        delete absent_day_url(absent_day.id)
      end.to change(AbsentDay, :count).by(-1)
    end
  end
end

require 'rails_helper'

RSpec.describe "Homepages", type: :request do
  let!(:subject) { create(:subject, day: I18n.l(Date.today, format: :day)) }
  let!(:absent_day) { student.absent_days.create(attributes_for(:absent_day)) }
  let!(:student) { create(:student) }
  let!(:absent_day_2) { student_2.absent_days.create(date: "2019/09/01", subject: "数学", supplementary: false) }
  let!(:student_2) { create(:student) }

  describe "GET /homepages" do
    login
    it "works successfully" do
      get root_path
      expect(response).to have_http_status(200)
      expect(response.body).to include subject.title
      expect(response.body).to include student.name
      expect(response.body).to include student_2.name
    end
  end
end

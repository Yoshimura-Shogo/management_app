require 'rails_helper'

RSpec.describe "Subjects::IndexSubjects", type: :request do
  describe "GET /subjects/index_subjects" do
    let!(:subject) { create(:subject) }
    login

    it "reponses successfully" do
      get subjects_path
      expect(response).to have_http_status(200)
      expect(response.body).to include(subject.title)
      expect(response.body).to include(subject.day)
      expect(response.body).to include(subject.students.count.to_s)
    end

    it "reponses successfully with params" do
      get subjects_path(day: "火")
      expect(response).to have_http_status(200)
      expect(response.body).to_not include(subject.title)
    end
  end
end

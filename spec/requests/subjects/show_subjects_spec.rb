require 'rails_helper'

RSpec.describe "Subjects::ShowSubjects", type: :request do
  describe "GET /subjects/:title" do
    let!(:subject) { create(:subject) }
    login
    it "reponses successfully" do
      get subject_path(subject.title)
      expect(response).to have_http_status(200)
      expect(response.body).to include(subject.title)
      expect(response.body).to include('example_student')
    end
  end
end

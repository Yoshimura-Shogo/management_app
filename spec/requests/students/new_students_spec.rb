require 'rails_helper'

RSpec.describe "new student", type: :request do
  context 'log in as admin' do
    login_as_admin
    describe "GET /students/new" do
      it "works　successfully" do
        get new_student_url
        expect(response).to have_http_status(200)
      end
    end

    describe "POST /students" do
      context "with valid params" do
        it 'works successfully and redirect to index' do
          post students_url, params: { student: attributes_for(:student) }
          expect(Student.find(1).grade_id).to eq(3)
          expect(response).to have_http_status(302)
          expect(response).to redirect_to student_url(1)
        end

        it 'sets grade_id automatically' do
          post students_url, params: { student: { name: "example student", grade: "中３" } }
          expect(Student.find(1).grade_id).to eq(3)
        end

        it 'creates a student successfully' do
          expect do
            post students_url, params: { student: attributes_for(:student) }
          end.to change(Student, :count).by(1)
        end
      end

      context 'with invalid params' do
        it 'contains error messages' do
          post students_url, params: { student: attributes_for(:student, :invalid) }
          expect(response.body).to include("を入力してください")
        end

        it 'does not create a student' do
          expect do
            post students_url, params: { student: attributes_for(:student, :invalid) }
          end.to change(Student, :count).by(0)
        end
      end
    end
  end

  context 'log in as non-admin' do
    login
    describe "GET /students/new" do
      it "works　unsuccessfully" do
        get new_student_url
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
      end
    end

    describe "POST /students" do
      it 'works unsuccessfully' do
        post students_url, params: { student: attributes_for(:student) }
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
      end

      it 'does not creates a student' do
        expect do
          post students_url, params: { student: attributes_for(:student) }
        end.to change(Student, :count).by(0)
      end
    end
  end
end

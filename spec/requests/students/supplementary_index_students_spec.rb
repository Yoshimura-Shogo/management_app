require 'rails_helper'

RSpec.describe "Supplementary students", type: :request do
  let!(:student) { create(:student) }
  let!(:other_student) { create(:student) }
  let!(:absent_day) { create(:absent_day, student: student) }
  let!(:absent_day_2) { create(:absent_day, supplementary: true, student: other_student) }

  describe "GET /students/supplementary" do
    login
    it "works　successfully" do
      get supplementary_students_url
      expect(response).to have_http_status(200)
      expect(response.body).to include(absent_day.date.to_s)
      expect(response.body).to include(student.name)
      expect(response.body).to include(student.grade)
      expect(response.body).to include(absent_day.subject)
      expect(response.body).to include("補習を完了する")
      expect(response.body).to_not include(other_student.name)
    end
  end
end

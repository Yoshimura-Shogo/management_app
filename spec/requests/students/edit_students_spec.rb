require 'rails_helper'

RSpec.describe "StudentEdits", type: :request do
  let!(:student) { create(:student) }

  context "log in as admin" do
    login_as_admin

    describe "GET /students/:id/edit" do
      it "responses successfully" do
        get edit_student_url(student.id)
        expect(response).to have_http_status(200)
        expect(response.body).to include student.name
      end
    end

    describe 'PUT /students/:id' do
      context ' with valid params' do
        it "works successfully and redirect to show" do
          put student_url(student.id), params: { student: attributes_for(:other_student) }
          expect(response).to have_http_status(302)
          expect(response).to redirect_to student_path(student.id)
        end

        it "updates student's name " do
          expect do
            put student_url(student.id), params: { student: attributes_for(:other_student) }
          end.to change { Student.find(student.id).name }.from(student.name).to('example_student_2')
        end

        it "updates grade_id" do
          expect do
            put student_url(student.id), params: { student: attributes_for(:other_student) }
          end.to change { Student.find(student.id).grade_id }.from(3).to(2)
        end
      end

      context 'with invalid params' do
        it 'not change student info' do
          expect do
            put student_url(student.id), params: { student: attributes_for(:student, :invalid) }
          end.not_to change(Student.find(student.id), :name)
        end
        it 'shows error messages' do
          put student_url(student.id), params: { student: attributes_for(:student, :invalid) }
          expect(response.body).to include("を入力してください")
        end
      end
    end
  end

  context "log in as non-admin" do
    login

    describe "GET /students/:id/edit" do
      it "responses unsuccessfully" do
        get edit_student_url(student.id)
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
      end
    end

    describe 'PUT /students/:id' do
      it "works unsuccessfully" do
        put student_url(student.id), params: { student: attributes_for(:other_student) }
        expect(response).to have_http_status(302)
        expect(response).to redirect_to root_path
      end

      it "doesn't updates student's info" do
        put student_url(student.id), params: { student: attributes_for(:other_student) }
        expect(Student.find(student.id).name).to_not eq('example_student_2')
      end
    end
  end
end

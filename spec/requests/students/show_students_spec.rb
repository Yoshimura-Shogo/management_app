require 'rails_helper'

RSpec.describe "Student", type: :request do
  describe "GET /student/:id" do
    let!(:student) { create(:student) }

    context 'log in as admin' do
      login_as_admin
      it "works　successfully" do
        get student_url(student.id)
        expect(response).to have_http_status(200)
        expect(response.body).to include(student.name)
        expect(response.body).to include(student.name)
        expect(response.body).to include(student.grade)
        student.subjects.each do |subject|
          expect(response.body).to include(subject.title)
        end
        expect(response.body).to include("編集")
        expect(response.body).to include("削除")
      end
    end

    context 'log in as non-admin' do
      login
      it "doesn't show 編集　and 削除" do
        get student_url(student.id)
        expect(response).to have_http_status(200)
        expect(response.body).to_not include("編集")
        expect(response.body).to_not include("削除")
      end
    end
  end
end

require 'rails_helper'

RSpec.describe "The absent students", type: :request do
  let!(:student) { create(:student) }
  let!(:absent_day) { create(:absent_day, student: student) }
  describe "GET /students/the_absent_day" do
    login
    it "works　successfully" do
      get the_absent_students_url
      expect(response).to have_http_status(200)
      expect(response.body).to include("#{absent_day.date}")
      expect(response.body).to include(student.name)
      expect(response.body).to include(student.grade)
      expect(response.body).to include(absent_day.subject)
      expect(response.body).to include("取消")
    end
  end
end

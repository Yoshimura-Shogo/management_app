require 'rails_helper'

RSpec.describe "Students", type: :request do
  describe "GET /students" do
    let!(:student) { create(:student) }
    login
    it "works　successfully" do
      get students_url
      expect(response).to have_http_status(200)
      expect(response.body).to include(student.name)
      expect(response.body).to include(student.grade)
    end
    it "works　successfully with params" do
      get students_url(grade: 2)
      expect(response).to have_http_status(200)
      expect(response.body).to_not include(student.name)
    end
  end
end

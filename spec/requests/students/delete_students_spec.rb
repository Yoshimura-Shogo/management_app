require 'rails_helper'

RSpec.describe "DeleteStudentsSpec.rbs", type: :request do
  describe "DELETE /students/:id" do
    let!(:student) { create(:student) }

    context "login as admin" do
      login_as_admin
      it "deletes a student" do
        expect do
          delete student_path(student.id)
        end.to change(Student, :count).by(-1)
        expect(response).to redirect_to students_path
      end
    end

    context "login as non-admin" do
      login
      it "doesn't delete a student" do
        expect do
          delete student_path(student.id)
        end.to change(Student, :count).by(0)
        expect(response).to redirect_to root_path
      end
    end
  end
end

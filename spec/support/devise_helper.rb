module DeviseHelper
  def login
    let(:user) { build(:user) }

    before do
      sign_in user
    end
  end

  def login_as_admin
    let(:admin) { build(:admin) }

    before do
      sign_in admin
    end
  end
end

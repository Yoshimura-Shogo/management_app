# == Schema Information
#
# Table name: subjects
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  day        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :subject do
    title { "中１　英語A" }
    day { "月" }

    after(:create) do |subject|
      subject.students << build(:student, name: "example_student")
    end
  end
  factory :subject_2, class: Subject do
    title { "中２　英語A" }
    day { "火" }
  end
end

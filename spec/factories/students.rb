# == Schema Information
#
# Table name: students
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  grade      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  grade_id   :integer
#

FactoryBot.define do
  factory :student do
    sequence(:name) { |n| "example_student#{n}" }
    grade { "中３" }
    grade_id { 3 }

    after(:create) do |student|
      student.subjects << build(:subject, title: "中１　英語A")
    end

    trait :invalid do
      name { nil }
    end
  end

  factory :other_student, class: Student do
    name { "example_student_2" }
    grade { "中２" }
    grade_id { 2 }
  end

end

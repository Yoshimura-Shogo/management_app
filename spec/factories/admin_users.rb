# == Schema Information
#
# Table name: admin_users
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  email           :string           not null
#  password_digest :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  admin           :boolean          default(FALSE), not null
#

FactoryBot.define do
  factory :admin_user do
    name { "MyString" }
    email { "MyString" }
    password_gigest { "MyString" }
  end
end

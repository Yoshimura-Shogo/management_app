# == Schema Information
#
# Table name: absent_days
#
#  id            :integer          not null, primary key
#  date          :date             not null
#  subject       :string           not null
#  supplementary :boolean          default(FALSE)
#  student_id    :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :absent_day do
    date { Date.today }
    subject { "英語" }
    supplementary { false }

    association :student
  end
end

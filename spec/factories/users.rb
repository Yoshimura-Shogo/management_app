# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string           default(""), not null
#

FactoryBot.define do
  factory :user do
    name                  { "user" }
    email                 { "user@example.com" }
    password              { "password" }
    password_confirmation { "password" }
  end

  factory :admin, class: User do
    name                  { "admin" }
    email                 { "admin@example.com" }
    password              { "password" }
    password_confirmation { "password" }
    admin                 { true }
  end
end

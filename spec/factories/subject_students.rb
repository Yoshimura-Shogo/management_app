# == Schema Information
#
# Table name: subject_students
#
#  id         :integer          not null, primary key
#  subject_id :integer          not null
#  student_id :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :subject_student do
    sequence(:subject_id) { |n| n }
    sequence(:student_id) { |n| n }
  end
end

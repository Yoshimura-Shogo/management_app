require 'rails_helper'

RSpec.describe "home", type: :system do
  let!(:student) { create(:student) }
  let!(:student_2) { create(:student) }
  let!(:the_absent) { create(:student) }
  let!(:supplementary) { create(:student) }
  let!(:absent_day) { create(:absent_day, date: Date.today, student: student) }
  let!(:absent_day_2) { create(:absent_day, student: student_2, supplementary: false) }
  let!(:subject) { create(:subject, day: I18n.l(Date.today, format: :day)) }


  let!(:user) { create(:user) }
  let!(:admin) { create(:admin) }

  context "log in as admin" do
    before do
      login_as(admin, scope: :user)
      visit root_path
      student.add_subject(subject)
    end
#
    scenario 'visit index' do
      expect(page).to have_title "MANAGEMENT APP"
      expect(page).to have_content subject.title
      expect(page).to have_content student.name
      expect(page).to have_content student_2.name
      expect(page).to have_content "管理者権限"
    end
#
    scenario "click on student.name" do
      click_on student.name
      expect(page).to have_current_path student_path(student.id)
    end

    scenario "click on student_2.name" do
      click_on student_2.name
      expect(page).to have_current_path student_path(student_2.id)
    end
  end
#
  context "log in as non-admin" do
    scenario 'visit index' do
      login_as(user, scope: :user)
      visit root_path
      expect(page).to_not have_content "管理者権限"
    end
  end
end

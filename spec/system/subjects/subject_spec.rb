require 'rails_helper'

RSpec.describe "index subject", type: :system do
  let!(:subject) { create(:subject) }
  let!(:user) { create(:user) }

  scenario 'visit index' do
    login_as(user, scope: :user)
    visit subjects_path
    expect(page).to have_title "科目一覧 | MANAGEMENT APP"
    expect(page).to have_link "subject-title"
    expect(page).to have_content subject.day
    expect(page).to have_content subject.students.count.to_s
    expect(page).to have_link "月"
    expect(page).to have_link "火"
    expect(page).to have_link "水"
    expect(page).to have_link "木"
    expect(page).to have_link "金"
    expect(page).to have_link "全科目"

    click_on 'subject-title'
    expect(page).to have_current_path subject_path(subject.title)
  end

  scenario "click on 月" do
    login_as(user, scope: :user)
    visit subjects_path
    click_on '火'
    expect(page).to_not have_link "subject-title"
  end

  scenario 'visit show' do
    login_as(user, scope: :user)
    visit subject_path(subject.title)
    expect(page).to have_title "#{subject.title} | MANAGEMENT APP"
    expect(page).to have_content subject.title
    expect(page).to have_link('example_student')

    click_on 'example_student'
    expect(page).to have_current_path student_path(subject.students.first.id)
  end
end

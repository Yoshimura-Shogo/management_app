require 'rails_helper'

RSpec.describe "new students", type: :system do
  let!(:student) { create(:student) }
  let!(:user) { create(:user) }
  before do
    login_as(user, scope: :user)
    visit students_path
  end

  scenario 'visit index' do
    expect(page).to have_title "生徒一覧 | MANAGEMENT APP"
    expect(page).to have_link student.name
    expect(page).to have_content student.grade
    expect(page).to have_link '中１'
    expect(page).to have_link '中２'
    expect(page).to have_link '中３'
  end

  scenario "click on student's name" do
    click_on student.name
    expect(page).to have_current_path student_path(student.id)
  end

  scenario "click on 中１" do
    click_on "中１"
    expect(page).to have_current_path students_path(grade: 1)
  end

  scenario "click on 中２" do
    click_on "中２"
    expect(page).to have_current_path students_path(grade: 2)
  end

  scenario "click on 中３" do
    click_on "中３"
    expect(page).to have_current_path students_path(grade: 3)
  end
end

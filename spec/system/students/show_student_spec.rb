require 'rails_helper'

RSpec.describe "show student", type: :system do
  let!(:student) { create(:student) }
  let!(:user) { create(:user) }
  let!(:admin) { create(:admin) }

  context "log in as admin" do
    before do
      login_as(admin, scope: :user)
      visit student_path(student.id)
    end

    scenario 'visit index' do
      expect(page).to have_title "生徒情報 | MANAGEMENT APP"
      expect(page).to have_content student.name
      expect(page).to have_content student.grade
      student.subjects.each do |subject|
        expect(page).to have_content(subject.title)
      end
      expect(page).to have_link '編集'
      expect(page).to have_link '削除'
    end

    scenario "click on 編集" do
      click_on "編集"
      expect(page).to have_current_path edit_student_path(student.id)
    end

    scenario "click on 削除" do
      click_on "削除"
      accept_confirm
      expect(page).to have_current_path students_path
      expect(page).to_not have_content student.name
    end
  end

  context "log in as non-admin" do
    scenario 'visit index' do
      login_as(user, scope: :user)
      visit student_path(student.id)
      expect(page).to_not have_link '編集'
      expect(page).to_not have_link '削除'
    end
  end
end

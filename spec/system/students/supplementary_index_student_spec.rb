require 'rails_helper'

RSpec.describe "supplementary index", type: :system do
  let!(:absent_day) { create(:absent_day) }
  let!(:user) { create(:user) }
  before do
    login_as(user, scope: :user)
    visit supplementary_students_path
  end

  scenario 'visit the_absent_students_path' do
    expect(page).to have_title "補習者未実施者一覧"
    expect(page).to have_content absent_day.date.to_s
    expect(page).to have_link absent_day.student_name
    expect(page).to have_content absent_day.student_grade
    expect(page).to have_content absent_day.subject
    expect(page).to have_link "補習を完了する"
  end

  scenario "click on student's name" do
    click_on absent_day.student_name
    expect(page).to have_current_path student_path(absent_day.student.id)
  end

  scenario "click on 補習完了" do
    click_on "補習を完了する"
    page.accept_confirm
    expect(page).to_not have_link absent_day.student.name
  end
end

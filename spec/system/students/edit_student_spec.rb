require 'rails_helper'

RSpec.describe "edit students", type: :system do
  let!(:student) { create(:student) }
  let!(:subject) { create(:subject) }
  let!(:subject_2) { create(:subject_2) }
  let!(:admin) { create(:admin) }
  let!(:user) { create(:user) }

  context "log in as admin" do
    scenario 'edits a student' do
      login_as(admin, scope: :user)
      visit edit_student_path(student.id)
      expect(page).to have_checked_field('student_subject_中１英語a')
      fill_in '名前', with: 'example'
      select '中２', from: "student_grade"
      uncheck 'student_subject_中１英語a'
      check 'student_subject_中２英語a'
      click_button '編集'

      expect(page).to have_current_path student_path(student.id)
      expect(page).to have_content 'example'
      expect(page).to have_content '中２'
      expect(page).to_not have_content '中１　英語A'
      expect(page).to have_content '中２　英語A'
      expect(page).to have_content '生徒情報を編集しました'
    end
  end

  context "log in as non-admin" do
    scenario 'edits a student' do
      login_as(user, scope: :user)
      visit edit_student_path(student.id)
      expect(page).to have_current_path root_path
      expect(page).to have_content '管理者のみ実行できます'
    end
  end
end

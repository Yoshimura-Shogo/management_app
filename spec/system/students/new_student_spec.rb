require 'rails_helper'

RSpec.describe "new students", type: :system do
  let!(:subject) { create(:subject) }
  let!(:user) { create(:user) }
  let!(:admin) { create(:admin) }

  context "log in as admin" do
    before do
      login_as(admin, scope: :user)
      visit new_student_path
    end

    scenario 'creates a student without subjects' do
      fill_in '名前', with: 'example'
      select '中１', from: 'student_grade'
      click_button '登録'

      expect(page).to have_current_path student_path(2)
      expect(page).to have_content 'example'
      expect(page).to have_content '中１'
      expect(page).to have_content '新規生徒を登録しました。'
    end

    scenario 'creates a student with subjects' do
      fill_in '名前', with: 'example'
      select '中１', from: 'student_grade'
      check "student_subject_中１英語a"
      click_button '登録'

      expect(page).to have_current_path student_path(2)
      expect(page).to have_content 'example'
      expect(page).to have_content '中１'
      expect(page).to have_content '中１　英語A'
      expect(page).to have_content '新規生徒を登録しました。'
    end
  end
  context "log in as non-admin" do
    scenario 'does notcreate a student' do
      login_as(user, scope: :user)
      visit new_student_path
      expect(page).to have_current_path root_path
      expect(page).to have_content '管理者のみ実行できます'
    end
  end
end

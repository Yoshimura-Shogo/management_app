require 'rails_helper'

RSpec.describe "absent day", type: :system do
  let!(:student) { create(:student) }
  let!(:absent_day) { create(:absent_day) }
  let!(:user) { create(:user) }

  scenario 'visit new' do
    login_as(user, scope: :user)
    visit new_absent_day_path(student_id: student.id)
    expect(page).to have_title "欠席登録 | MANAGEMENT APP"
    expect(page).to have_content student.name
    expect(page).to have_content student.grade
    select "英語"
    click_on '登録'
    expect(page).to have_current_path the_absent_students_path
    expect(page).to have_content "欠席情報を作成しました。"
    expect(page).to have_content student.name
  end
end

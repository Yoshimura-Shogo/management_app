class CreateAbsentDays < ActiveRecord::Migration[5.2]
  def change
    create_table :absent_days do |t|
      t.date :absent_at, null: false
      t.string :subject, null: false
      t.boolean :supplementary, default: false
      t.references :student, foreign_key: true

      t.timestamps
    end
    add_index :absent_days, [:absent_at, :student_id]
  end
end

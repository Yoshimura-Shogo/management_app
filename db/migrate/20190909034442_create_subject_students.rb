class CreateSubjectStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :subject_students do |t|
      t.integer :subject_id, null: false
      t.integer :student_id, null: false

      t.timestamps
    end
    add_index :subject_students, :subject_id
    add_index :subject_students, :student_id
    add_index :subject_students, [:subject_id, :student_id], unique: true
  end
end

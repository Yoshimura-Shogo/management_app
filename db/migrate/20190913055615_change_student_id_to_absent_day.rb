class ChangeStudentIdToAbsentDay < ActiveRecord::Migration[5.2]
  def change
    change_column :absent_days, :student_id, :integer, null: false
  end
end

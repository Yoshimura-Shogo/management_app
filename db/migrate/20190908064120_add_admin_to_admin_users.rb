class AddAdminToAdminUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :admin_users, :admin, :boolean, default: false
    change_column :admin_users, :admin, :boolean, null: false
  end
  def down
    remove_column :admin_users, :admin
  end
end

class RenameAbsentAtColumnToDate < ActiveRecord::Migration[5.2]
  def change
    rename_column :absent_days, :absent_at, :date
  end
end

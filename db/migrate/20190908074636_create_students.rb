class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name, null: false
      t.string :grade, null: false

      t.timestamps
      t.index :name, unique: true
    end
  end
end

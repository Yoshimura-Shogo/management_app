class Rename < ActiveRecord::Migration[5.2]
  def change
    rename_column :subjects, :subject, :title
  end
end

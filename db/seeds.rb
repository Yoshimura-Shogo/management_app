Faker::Config.locale = :ja
#科目
Subject.create!(title:"中１　英語A", day: "月")
Subject.create!(title:"中１　数学A", day: "火")
Subject.create!(title:"中１　国語A", day: "水")
Subject.create!(title:"中１　英語B", day: "月")
Subject.create!(title:"中１　数学B", day: "火")
Subject.create!(title:"中１　国語B", day: "水")
Subject.create!(title:"中２　英語A", day: "木")
Subject.create!(title:"中２　数学A", day: "金")
Subject.create!(title:"中２　国語A", day: "月")
Subject.create!(title:"中２　英語B", day: "木")
Subject.create!(title:"中２　数学B", day: "金")
Subject.create!(title:"中２　国語B", day: "月")
Subject.create!(title:"中３　英語A", day: "火")
Subject.create!(title:"中３　数学A", day: "水")
Subject.create!(title:"中３　国語A", day: "木")
Subject.create!(title:"中３　英語B", day: "火")
Subject.create!(title:"中３　数学B", day: "水")
Subject.create!(title:"中３　国語B", day: "木")

#　生徒
5.times do
  name = Faker::Name.name
  grade = "中１"
  grade_id = 1
  student =  Student.create!(name: name, grade: grade, grade_id: grade_id)
  3.times do |n|
    student.subjects << Subject.find(n+1)
  end
  absent_day = Random.rand(Date.parse("2019/08/01")..Date.parse("2019/09/20"))
  subject = ["英語", "国語", "数学"].sample
  student.absent_days.create!(date: absent_day , subject: subject, supplementary: false )
end

5.times do
  name = Faker::Name.name
  grade = "中１"
  grade_id = 1
  student =  Student.create!(name: name, grade: grade, grade_id: grade_id)
  3.times do |n|
    student.subjects << Subject.find(n+4)
  end
  absent_day = Random.rand(Date.parse("2019/08/01")..Date.parse("2019/09/20"))
  subject = ["英語", "国語", "数学"].sample
  student.absent_days.create!(date: absent_day , subject: subject, supplementary: false )
end

5.times do
  name = Faker::Name.name
  grade = "中２"
  grade_id = 2
  student =  Student.create!(name: name, grade: grade, grade_id: grade_id)
  3.times do |n|
    student.subjects << Subject.find(n+7)
  end
end

5.times do
  name = Faker::Name.name
  grade = "中２"
  grade_id = 2
  student =  Student.create!(name: name, grade: grade, grade_id: grade_id)
  3.times do |n|
    student.subjects << Subject.find(n+10)
  end
end

5.times do
  name = Faker::Name.name
  grade = "中３"
  grade_id = 3
  student =  Student.create!(name: name, grade: grade, grade_id: grade_id)
  3.times do |n|
    student.subjects << Subject.find(n+13)
  end
end

5.times do
  name = Faker::Name.name
  grade = "中３"
  grade_id = 3
  student =  Student.create!(name: name, grade: grade, grade_id: grade_id)
  3.times do |n|
    student.subjects << Subject.find(n+15)
  end
end

# ユーザ
User.create!(name: "管理者", email: "admin@example.com",
            password: "password", password_confirmation: "password", admin: true)
